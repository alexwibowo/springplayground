package com.wibowo.alex.crumbs;

import com.wibowo.alex.crumbs.event.EventServices;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * User: alexwibowo
 */
public class JavaMain {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext();
//        annotationConfigApplicationContext.register(CliConfig.class);
        annotationConfigApplicationContext.refresh();

        EventServices bean = annotationConfigApplicationContext.getBean(EventServices.class);
        System.out.println(bean);
    }
}
