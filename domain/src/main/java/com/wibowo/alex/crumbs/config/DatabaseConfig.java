package com.wibowo.alex.crumbs.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

/**
 * User: alexwibowo
 */
@Configuration
public class DatabaseConfig {
    @Bean
    public DataSource dataSource(
            @Value("${database.driver}") String databaseDriver,
            @Value("${database.url}") String databaseURL,
            @Value("${database.username}") String databaseUsername,
            @Value("${database.password}") String databasePassword
    ) {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName(databaseDriver);
        driverManagerDataSource.setUrl(databaseURL);
        driverManagerDataSource.setUsername(databaseUsername);
        driverManagerDataSource.setPassword(databasePassword);
        return driverManagerDataSource;
    }
}
