package com.wibowo.alex.crumbs.user

import com.wibowo.alex.crumbs.config.DatabaseConfig
import com.wibowo.alex.crumbs.config.PersistenceConfig
import com.wibowo.alex.crumbs.config.PropertiesConfig
import org.joda.time.DateTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ContextConfiguration
import org.springframework.transaction.annotation.Transactional
import spock.lang.Specification

/**
 * User: alexwibowo
 */
@ContextConfiguration(classes = [
    PersistenceConfig.class,
    DatabaseConfig.class,
    PropertiesConfig.class
])
@Transactional
class UserRepositorySpec extends Specification {

    @Autowired
    private UserRepository userRepository

    def "test saving"() {
        given:
        def user = new User(
                username: 'wibowoa',
                createdAt: DateTime.now()
        )

        when:
        def saved = userRepository.save(user)

        then:
        assert saved.id != null

    }




}