package com.wibowo.alex.crumbs.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * User: alexwibowo
 */
@Configuration
@PropertySources({
        @PropertySource("classpath:conf/database.properties")
})
public class PropertiesConfig {

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
        propertySourcesPlaceholderConfigurer.setIgnoreResourceNotFound(false);
        propertySourcesPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(false);
        return propertySourcesPlaceholderConfigurer;
    }
}
