CREATE USER krumz WITH PASSWORD 'krumz';

CREATE ROLE krumz_read_write;
GRANT krumz_read_write TO krumz;
CREATE TABLESPACE krumz_dbspace OWNER krumz location '/Users/alexwibowo/postgresql/krumz';
CREATE DATABASE krumz WITH OWNER krumz TABLESPACE krumz_dbspace;

-- Select database 'krumz' first
-- Create schema within the database 'krumz'
CREATE SCHEMA KRUMZ;
GRANT ALL PRIVILEGES ON SCHEMA krumz to krumz_read_write;
GRANT ALL PRIVILEGES ON ALL TABLES    IN SCHEMA krumz TO krumz_read_write;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA krumz TO krumz_read_write;

-- set default schema for the user
ALTER USER krumz SET search_path TO 'krumz';
-- SET SCHEMA KRUMZ;
-- SET INITIAL SCHEMA KRUMZ;
