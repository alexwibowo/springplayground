package com.wibowo.alex.crumbs;

import com.wibowo.alex.crumbs.event.EventServices;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * User: alexwibowo
 */
public class XmlMain {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        applicationContext.refresh();

        EventServices bean = applicationContext.getBean(EventServices.class);
              System.out.println(bean);
    }
}
