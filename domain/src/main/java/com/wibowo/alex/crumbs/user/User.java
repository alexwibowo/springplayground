package com.wibowo.alex.crumbs.user;

import com.wibowo.alex.crumbs.event.Event;
import com.wibowo.alex.crumbs.framework.domain.AbstractModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.List;

/**
 * User: alexwibowo
 */
@Entity
@Table(name = "KRUMZ_USER")
@SequenceGenerator(name = "ID_GENERATOR", sequenceName = "KRUMZ_USER_SEQ")
public class User extends AbstractModel {

    @Column(name = "USERNAME")
    private String username;

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "user"
    )
    private List<Event> events;

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(final List<Event> events) {
        this.events = events;
        events.forEach(e -> e.setUser(this));
    }

    public User addEvent(final Event newEvent) {
        newEvent.setUser(this);
        this.events.add(newEvent);
        return this;
    }
}
