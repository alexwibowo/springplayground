package com.wibowo.alex.crumbs.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

/**
 * User: alexwibowo
 */
@Configuration
@PropertySources({
        @PropertySource("classpath:conf/database.properties")
})
public class PropertiesConfig {

}
