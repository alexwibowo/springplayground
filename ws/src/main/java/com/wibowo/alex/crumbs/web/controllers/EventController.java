package com.wibowo.alex.crumbs.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * User: alexwibowo
 */
@Controller
@RequestMapping("/events")
public class EventController {

    @RequestMapping("/index")
    @ResponseBody
    public String index() {
        return "Hey!";
    }
}
