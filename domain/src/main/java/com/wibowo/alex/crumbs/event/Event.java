package com.wibowo.alex.crumbs.event;

import com.wibowo.alex.crumbs.framework.domain.AbstractModel;
import com.wibowo.alex.crumbs.user.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * User: alexwibowo
 */
@Entity
@Table(name = "EVENT")
@SequenceGenerator(name = "ID_GENERATOR", sequenceName = "EVENT_SEQ")
public class Event extends AbstractModel {

    @Column(name = "NAME")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USER_ID", nullable= false)
    private User user;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }
}
