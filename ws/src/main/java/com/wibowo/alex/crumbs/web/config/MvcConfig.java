package com.wibowo.alex.crumbs.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * User: alexwibowo
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.wibowo.alex.crumbs.web.controllers")
public class MvcConfig extends WebMvcConfigurerAdapter{
}
