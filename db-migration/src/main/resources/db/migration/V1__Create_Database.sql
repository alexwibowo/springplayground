CREATE SEQUENCE krumz_user_seq
 	INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE krumz_user_seq OWNER TO krumz_read_write;

CREATE TABLE krumz_user(
	id		NUMERIC(32,0) 	NOT NULL PRIMARY KEY,
	username	VARCHAR(30) 	NOT NULL,
	created_at	TIMESTAMP	with time zone NOT NULL,
	UNIQUE(username)
);

CREATE SEQUENCE event_seq
 	INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE event_seq OWNER TO krumz_read_write;

CREATE TABLE event (
	id 		NUMERIC(32,0)   NOT NULL PRIMARY KEY,
	created_at	TIMESTAMP	with time zone NOT NULL,
	user_id		NUMERIC(32,0)	NOT NULL REFERENCES krumz_user(id),
	latitude	NUMERIC(5,0),
	longitude	NUMERIC(5,0)
);
