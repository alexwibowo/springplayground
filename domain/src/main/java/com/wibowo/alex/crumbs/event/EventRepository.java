package com.wibowo.alex.crumbs.event;

import org.springframework.data.repository.CrudRepository;

/**
 * User: alexwibowo
 */
public interface EventRepository extends CrudRepository<Event, Long> {
}
