package com.wibowo.alex.crumbs.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * User: alexwibowo
 */
@Configuration
@Import({
        PropertiesConfig.class
})
public class MainConfig {

}
