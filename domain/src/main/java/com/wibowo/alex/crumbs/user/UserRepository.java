package com.wibowo.alex.crumbs.user;

import org.springframework.data.repository.CrudRepository;

/**
 * User: alexwibowo
 */
public interface UserRepository extends CrudRepository<User, Long> {
}
